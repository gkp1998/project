#include "gtest/gtest.h"


class TF {
public:
   // TF();
    int add()
    {
        int t=1;

        return t;
    }

};


namespace {

    class TFTest : public ::testing::Test {
    protected:

        static void SetUpTestCase()
        {

        }
        virtual void SetUp() override {
            tf.add();
        }
        virtual void TearDown() override
        {

        }

        TF tf;
    };

    TEST_F(TFTest, tfadd) {
        EXPECT_EQ(2, tf.add());
    }
}