![coverage](https://gitlab.com/gkp1998/Project/badges/master/coverage.svg?style=flat)
![build status](https://img.shields.io/gitlab/pipeline/gkp1998/Project.svg)
![pipeline status](https://gitlab.com/gkp1998/Project/badges/master/pipeline.svg)


## Build Status
|               |           |
|:-------------:|:---------:|
| Build Status  |[![build status](https://img.shields.io/gitlab/pipeline/gkp1998/Project.svg)](https://gitlab.com/gkp1998/project)|
| Pipeline|[![pipeline status](https://gitlab.com/gkp1998/Project/badges/master/pipeline.svg)](https://gitlab.com/gkp1998/project/pipelines)

#Diff
|   OS/Platform   |   Debug        |     Test      |    Release  |
|:-------------:|:---------:|:---------:|:-------:|:----:|
|Mac OS|